# 2022 Advent of Code

## Day 1: Calorie Counting
https://adventofcode.com/2022/day/1

Split the input by blank lines, split again by new lines, add up the totals. Find the largest N.

## Day 2: Rock Paper Scissors
https://adventofcode.com/2022/day/2

Parse the input, the outcome using some arbitrary rules, sum up the results.

## Day 3: Rucksack Reorganization
https://adventofcode.com/2022/day/3

Find common characters in strings, assign them a value, and sum that.

## Day 4: Camp Cleanup
https://adventofcode.com/2022/day/4

Parse ranges of Integers out of strings, check if they overlap or are completely contained within the other.
