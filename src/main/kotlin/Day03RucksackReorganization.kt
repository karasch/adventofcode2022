package net.marjean.adventofcode

fun main(){

    /*
    val input = listOf(
        "vJrwpWtwJgWrhcsFMMfFFhFp",
        "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
        "PmmdzqPrVvPwwTWBwg",
        "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
        "ttgJtRGJQctTZtZT",
        "CrZsJsPPZsGzwwsLwLmpwMDw",
    )

     */
    //Example input.
    // Problem 1 value == 157
    // Problem 2 value == 70


    val input = Utilities.readInputToList("/Day03RucksackReorganization.txt") ?: return

    println("Problem 1")
    println(
        input.sumOf { findPriority(findCommon(breakString(it))) }
    )

    println("Problem 2")
    println(
        input.chunked(3).sumOf { findPriority(findCommon(it[0], it[1], it[2])) }
    )


}

fun findPriority(item: Char): Int {
    val lower = CharRange('a','z')
    return if(lower.contains(item))
        item.minus('a')+1
    else
        item.minus('A')+27
}

fun findCommon(pr: Pair<String, String,>): Char = findCommon(pr.first, pr.second)

fun findCommon(a: String, b: String): Char =
    a.first { b.contains(it) }

fun findCommon(a: String, b: String, c: String): Char =
    a.first { b.contains(it) && c.contains(it) }

fun breakString(input: String): Pair<String, String>{
    if(input.length % 2 == 1) throw IllegalArgumentException()

    return Pair(input.take(input.length/2), input.takeLast(input.length/2))
}
