package net.marjean.adventofcode

import java.lang.Integer.parseInt

fun main(){

    val inputStrings = Utilities.readMultilineInputToList("/Day01CalorieCounting.txt") ?: return

    val input = inputStrings.map { it.split("\n", "\r\n") }

    val sums = input.map { it.sumOf { element -> parseInt(element) } }

    println("Part 1")
    println(sums.maxOrNull())

    println("Part 2")
    println(sums.sorted().takeLast(3).sum())
}