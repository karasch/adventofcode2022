package net.marjean.adventofcode

object Utilities {
    fun readInputToList(filename: String): List<String>? =
        Utilities::class.java.getResourceAsStream(filename)
            ?.bufferedReader()?.readLines()

    /**
     * Reads lines from a file into a List of Strings, records separated by blank lines
     */
    fun readMultilineInputToList(filename: String): List<String>? =
        Utilities::class.java.getResourceAsStream(filename)
            ?.bufferedReader()?.readText()
            ?.trim()?.split("\n\n", "\r\n\r\n")

}