package net.marjean.adventofcode

import java.lang.Integer.parseInt

fun main(){
/*
    val input = listOf(
        "2-4,6-8",
        "2-3,4-5",
        "5-7,7-9",
        "2-8,3-7",
        "6-6,4-6",
        "2-6,4-8",
    )
    // Example Input
    // Problem 1 == 2
    // Problem 2 == 4


 */

    val input = Utilities.readInputToList("/Day04CampCleanup.txt") ?: return

    println("Problem 1")
    println(input.count { oneContainsOther( getRanges(it) ) } )

    println("Problem 2")
    println(input.count { overlapAtAll(getRanges(it)) })
}

fun getRanges(input: String): Pair<Pair<Int, Int>, Pair<Int, Int>>{
    val pairs = input.split(",", limit = 2)
    val first = pairs.first().split("-", limit = 2)
    val second = pairs.last().split("-", limit = 2)
    return Pair(Pair(parseInt(first.first()), parseInt(first.last())), Pair(parseInt(second.first()), parseInt(second.last())))
}

fun oneContainsOther(input: Pair<Pair<Int, Int>, Pair<Int, Int>>): Boolean = oneContainsOther(input.first, input.second)

fun oneContainsOther(a: Pair<Int, Int>, b: Pair<Int, Int>): Boolean =
    (a.first <= b.first && a.second >= b.second) || (a.first >= b.first && a.second <= b.second)

fun overlapAtAll(input: Pair<Pair<Int, Int>, Pair<Int, Int>>) =
    overlapAtAll(input.first, input.second)

fun overlapAtAll(a: Pair<Int, Int>, b: Pair<Int, Int>): Boolean =
    oneContainsOther(a, b) || a.first in b.first..b.second || a.second in b.first..b.second


