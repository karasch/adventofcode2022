package net.marjean.adventofcode

fun main(){
    val inputLines = Utilities.readInputToList("/Day02RockPaperScissors.txt") ?: return

    val input = inputLines.map { it.split(" ") }

    println("Problem 1")
    println(input.sumOf { scoreRound(it.first(), it.last()) })

    println("Problem 2")
    println(input.sumOf { scoreRound(it.first(), decodeChoice(it.first(), it.last())) })
}

/**
 * X = lose,
 * Y = draw,
 * Z = win
 *
 * Technically, this could just return an Int.  We already have all the information we need to score,
 * but I wrote this first, and it works.
 */
fun decodeChoice(elfChoice: String, outcome: String): String =
    when(elfChoice){
        "A" -> {
            when(outcome){
                "X" -> "Z"
                "Y" -> "X"
                "Z" -> "Y"
                else -> throw IllegalArgumentException()
            }
        }
        "B" -> {
            when(outcome){
                "X" -> "X"
                "Y" -> "Y"
                "Z" -> "Z"
                else -> throw IllegalArgumentException()
            }
        }
        "C" -> {
            when(outcome){
                "X" -> "Y"
                "Y" -> "Z"
                "Z" -> "X"
                else -> throw IllegalArgumentException()
            }
        }
        else -> throw IllegalArgumentException()
    }



fun scoreRound(elfChoice: String, myChoice: String): Int =
    when (myChoice){
        "X" -> { 1 +
                when(elfChoice){
                    "A" -> 3
                    "B" -> 0
                    "C" -> 6
                    else -> throw IllegalArgumentException()
                }
        }
        "Y" -> { 2 +
                when(elfChoice){
                    "A" -> 6
                    "B" -> 3
                    "C" -> 0
                    else -> throw IllegalArgumentException()
                }
        }
        "Z" -> { 3 +
                when(elfChoice){
                    "A" -> 0
                    "B" -> 6
                    "C" -> 3
                    else -> throw IllegalArgumentException()
                }
        }
        else -> throw IllegalArgumentException()
    }
